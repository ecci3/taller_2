package com.ecci.taller_2

import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import kotlinx.android.synthetic.main.dialog_terms_conditions.*

class TermsConditionsDialog(context: Context, private val intent: Intent): Dialog(context) {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.dialog_terms_conditions)

        buttonOk.setOnClickListener {
            context.startActivity(intent)
        }
        buttonCancel.setOnClickListener {
            this.dismiss()
        }
    }
}