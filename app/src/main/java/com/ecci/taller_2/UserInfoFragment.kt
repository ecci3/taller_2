package com.ecci.taller_2

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_user_info.*

class UserInfoFragment(private val name: String?, private val lastName: String?, private val docType: String?, private val docNumber: String?,
                       private val birthday: String?, private val hobbies: String?) : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_user_info, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        //Se setean los texview con los valores de la información del usuario
        textViewNameInfo.text = name
        textViewLastNameInfo.text = lastName
        textViewDocTypeInfo.text = docType
        textViewNumDocInfo.text = docNumber
        textViewBirthdayInfo.text = birthday
        textViewHobbiesInfo.text = hobbies
    }
}