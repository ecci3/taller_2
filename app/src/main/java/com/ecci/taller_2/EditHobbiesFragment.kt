package com.ecci.taller_2

import android.app.AlertDialog
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_edit_hobbies.*


class EditHobbiesFragment( private val hobbies: String?, private val hobbiesCheckedArray: BooleanArray?, val callback: (String) -> Unit) : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_edit_hobbies, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        //Se setea el text view con los hobbies del usuario
        textViewHobbiesInfo.text = hobbies
        //Mostrar dialogo para seleccionar los hobbies
        imageButtonEdit.setOnClickListener {
            showHobbiesDialog()
        }
    }

    private fun showHobbiesDialog(){
        //Se crea un objeto de tipo data para traer los tipos de documentos
        val data = Data("")
        //Arreglo de Hobbies
        val hobbiesArray = arrayOf(data.read, data.listen, data.write,
                data.TV, data.sport)
        //Se crea el Alert Dialog y se modifican los atributos
        val builder = AlertDialog.Builder(requireContext())
        builder.setTitle(R.string.select_your_hobbies)
        builder.setMultiChoiceItems(hobbiesArray, hobbiesCheckedArray)
        {_, position, isChecked ->
            //Se almacenan los hobbies selecionados y los que no en el array
            hobbiesCheckedArray?.set(position, isChecked)

        }
        builder.setPositiveButton(R.string.acept) { _, _ ->
            textViewHobbiesInfo.setText("")
            // Si el arreglo es diferente de null
            // se recorre el se agregan los hobbies cheked al textViewHobbies
            if (hobbiesCheckedArray != null) {
                for (i in hobbiesCheckedArray.indices) {
                    val checked = hobbiesCheckedArray.get(i)
                    if (checked) {
                        textViewHobbiesInfo.setText(textViewHobbiesInfo.text.toString().plus(hobbiesArray[i].plus(", ")))
                    }
                }
            }
            //Se envia la info al activity mediante una función calback
            callback(textViewHobbiesInfo.text.toString())
        }
        builder.setNegativeButton(R.string.cancel) { _, _ ->
        }
        builder.show()
    }
}