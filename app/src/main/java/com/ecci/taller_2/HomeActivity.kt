package com.ecci.taller_2

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_home.*

class HomeActivity : AppCompatActivity() {
    private var hobbies: String =""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
        hobbies = intent.getStringExtra(R.string.hobbies_key.toString()).toString()
        showUserInfoFragment()
        buttonInfo.setOnClickListener {
            showUserInfoFragment()
        }
        buttonEdit.setOnClickListener {
            showEditHobbiesFragment()
        }
    }

    private fun showUserInfoFragment(){
        //Se crea un fragmento con sus respectivos parametros
        val fragment = UserInfoFragment(intent.getStringExtra(R.string.name_key.toString()), intent.getStringExtra(R.string.lastName_key.toString()),
            intent.getStringExtra(R.string.docType_key.toString()), intent.getStringExtra(R.string.numDoc_key.toString()),
            intent.getStringExtra(R.string.birthday_key.toString()), hobbies)
        //Se crea una transacción
        val transaction = supportFragmentManager.beginTransaction()
        //Se le agrega el contenedor y el fragmento
        transaction.replace(R.id.container, fragment, null)
        // Se completa la transacción
        transaction.commit()
    }

    private fun showEditHobbiesFragment(){
        //Se crea un fragmento con sus respectivos parametros
        val fragment = EditHobbiesFragment(hobbies, intent.getBooleanArrayExtra(R.string.hobbiesCheckedArray_key.toString()),{ hobbiesEdited ->
                hobbies = hobbiesEdited
        })
        //Se crea una transacción
        val transaction = supportFragmentManager.beginTransaction()
        //Se le agrega el contenedor y el fragmento
        transaction.replace(R.id.container, fragment, null)
        // Se completa la transacción
        transaction.commit()
    }
}