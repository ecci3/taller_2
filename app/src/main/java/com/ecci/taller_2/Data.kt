package com.ecci.taller_2

data class Data(var hi: String) {
    val cc = "Cédula de ciudadania"
    val ti = "Tarjeta de identidad"
    val rc = "Registro civil"
    val ce = "Cedula de extranjeria"
    val passport = "Pasaporte"

    val read = "Leer"
    val listen = "Escuchar musica"
    val write = "Escribir"
    val TV = "Ver television"
    val sport = "Deporte"
}